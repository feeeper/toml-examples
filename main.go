package main

import (
	burntToml "github.com/BurntSushi/toml"
	naoinaToml "github.com/naoina/toml"
	"log"
	"io/ioutil"
	"github.com/fatih/stopwatch"
)

type Config struct {
	ID string
	Index int
	GUID string
	IsActive bool
	Balance string
	Picture string
	Age int
	EyeColor string
	Name struct {
		First string
		Last string
	}
	Company string
	Email string
	Phone string
	Address string
	About string
	Registered string
	Latitude string
	Longitude string
	Tags []string
	Range []int
	Friends []struct {
		ID int
		Name string
	}
	Greeting string
	FavoriteFruit string
	SpecialField string `toml:"my_tagged_filed"`
}

type Config2 struct {
	ID string
	Index int
	GUID string
	IsActive bool
	Balance string
	Picture string
	Age int
	EyeColor string
	Name struct {
		First string
		Last string
	}
	Company string
	Email string
	Phone string
	Address string
	About string
	Registered string
	Latitude string
	Longitude string
	Tags []string
	Range []int
	Friends []struct {
		ID int
		Name string
		Phone string
		Company string
		Email string
	}
	Greeting string
	FavoriteFruit string
}

type Toml struct {
	Toml []Config2
}

func main()  {
	sw := stopwatch.New()
	sw.Start(0)
	config := Config{}
	if _, e := burntToml.DecodeFile("config.toml", &config); e != nil {
		log.Fatal(e)
	}
	log.Printf("Burnt. Simple decode: %s", sw.ElapsedTime().String())

	sw.Reset()
	sw.Start(0)
	bytes, err := ioutil.ReadFile("config.toml")
	if err != nil {
		log.Fatal(err)
	}

	err = naoinaToml.Unmarshal(bytes, &config)
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("Naoina. Simple decode: %s", sw.ElapsedTime().String())

	sw.Reset()
	sw.Start(0)
	for i := 0; i < 10; i++ {
		parseLargeFile_Burnt()
	}
	log.Printf("Burnt Large: %s", sw.ElapsedTime().String())

	sw.Reset()
	sw.Start(0)
	for i := 0; i < 10; i++ {
		parseLargeFile_Naoina1()
	}
	log.Printf("Naoina Large 1: %s", sw.ElapsedTime().String())

	sw.Reset()
	sw.Start(0)
	bytes, err = ioutil.ReadFile("large.toml")
	if err != nil {
		log.Fatal(err)
	}
	for i := 0; i < 10; i++ {
		parseLargeFile_Naoina2(bytes)
	}
	log.Printf("Naoina Large 2: %s", sw.ElapsedTime().String())

	sw.Stop()
}

func parseLargeFile_Burnt() {
	large_1 := Toml{}
	if _, err := burntToml.DecodeFile("large.toml", &large_1); err != nil {
		log.Fatal(err)
	}
}

func parseLargeFile_Naoina1() {
	large_1 := Toml{}
	var bytes []byte
	bytes, err := ioutil.ReadFile("large.toml")
	if err != nil {
		log.Fatal(err)
	}

	if err := naoinaToml.Unmarshal(bytes, &large_1); err != nil {
		log.Fatal(err)
	}
}

func parseLargeFile_Naoina2(bytes []byte) {
	large_1 := Toml{}
	if err := naoinaToml.Unmarshal(bytes, &large_1); err != nil {
		log.Fatal(err)
	}
}